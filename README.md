# human_models

In this repository you will find URDF models of DHMs (Digital HUman Models). 
Every model has a different number of degrees of freedom, different joints and links, and potentially serves to a different purpose.
You will find below a short description of each model, with its features and its intended purpose.

## Installation

To use the models in your machine, do:

TODO

## How to use them in your code

TODO

## Generating or updating a model

In some cases, we are interested to change the properties of the DHM. In \scripts you will find a DHM generator that scales the model with respect to heigth and weight of the human.

TODO

## List of models

TODO 
